import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  /* Ruta de inicio */
  { path: '', redirectTo: 'login', pathMatch: 'full' },

  /* Rutas de Autentiacion  */

  { path: 'login', loadChildren: './pages/auth/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/auth/register/register.module#RegisterPageModule' },
  { path: 'passwordrecover', loadChildren: './pages/auth/password-recover/password-recover.module#PasswordRecoverPageModule' },

  /* Rutas del Dashboard */
  /* Ruta de inicio del Dashboard */
  { path: 'landing', loadChildren: './pages/user/dashboard/landing/landing.module#LandingPageModule' },
  { path: 'menu', loadChildren: './pages/global/menu/menu.module#MenuPageModule' },
  { path: 'start-page', loadChildren: './pages/user/dashboard/start-page/start-page.module#StartPagePageModule' },

  /* Rutas de la Orden */
  { path: 'trackorderlist', loadChildren: './pages/user/dashboard/order/track-order-list/track-order-list.module#TrackOrderListPageModule' },
  { path: 'trakingorder', loadChildren: './pages/user/dashboard/order/traking-order/traking-order.module#TrakingOrderPageModule' },

  { path: 'order-history', loadChildren: './pages/user/dashboard/order/order-history/order-history.module#OrderHistoryPageModule' },
  { path: 'trackorder', loadChildren: './pages/user/dashboard/order/track-order/track-order.module#TrackOrderPageModule' },

  /* Rutas De Productos */
  { path: 'user-config', loadChildren: './pages/user/dashboard/user-config/user-config.module#UserConfigPageModule' },
  { path: 'category', loadChildren: './pages/user/dashboard/product/category/category.module#CategoryPageModule' },
  { path: 'category-details', loadChildren: './pages/user/dashboard/product/category-details/category-details.module#CategoryDetailsPageModule' },
  { path: 'image-modal', loadChildren: './pages/global/image-modal/image-modal.module#ImageModalPageModule' },
  { path: 'product-details', loadChildren: './pages/user/dashboard/product/product-details/product-details.module#ProductDetailsPageModule' },
  { path: 'reqsdata', loadChildren: './pages/user/dashboard/order/request-data/request-data.module#RequestDataPageModule' },
  { path: 'request-details', loadChildren: './pages/user/dashboard/order/request-details/request-details.module#RequestDetailsPageModule' },
  { path: 'orderhistorydetail/:id', loadChildren: './pages/user/dashboard/order/order-history-details/order-history-details.module#OrderHistoryDetailsPageModule' },
  { path: 'order-repeat', loadChildren: './pages/user/dashboard/order/order-repeat/order-repeat.module#OrderRepeatPageModule' },
  { path: 'maps-native', loadChildren: './pages/user/dashboard/order/maps-native/maps-native.module#MapsNativePageModule' },
  { path: 'soporte', loadChildren: './pages/global/soporte/soporte.module#SoportePageModule' },
  { path: 'faq', loadChildren: './pages/global/faq/faq.module#FaqPageModule' },
  { path: 'sub-category', loadChildren: './pages/user/dashboard/product/sub-category/sub-category.module#SubCategoryPageModule' },
  { path: 'sub-category-details', loadChildren: './pages/user/dashboard/product/sub-category-details/sub-category-details.module#SubCategoryDetailsPageModule' },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
