
import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { NgForm } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  hideorshow = 'eye';
  type = 'password';
  isLoging: 'nologueado';
  public form: any = [];

  constructor(
    private modalController: ModalController,
    private auth: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private router: Router,
    private storage: Storage,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder
  ) {


  }

  ngOnInit() {

  }

  goToResetPassword() {
    this.router.navigate(['passwordrecover']);
  }

  createAccount() {
    this.router.navigate(['register']);
  }

  login(form: NgForm) {
    if (form.value.email !== undefined && form.value.email !== '') {
      this.auth
        .login({
          email: form.value.email,
          password: form.value.password
        })
        .subscribe(
          (data) => {
            this.alertService.presentToast('Bienvenido:  ' + data.response.user.name);
            if (data.response.sucess === 1) {
              this.storage.set('user', data.response.user);
              this.storage.set('isLoging', 'logueado');

            }
          },
          (error) => {
            console.log(error);
            this.alertService.presentToast(error.statusText);
          },
          () => {
            this.router.navigate(['menu/landing']);
          }
        );
    }
  }

  ocultarMostrarPassword() {
    const state = this.type;

    if (state === 'password') {
      this.type = 'text';
      this.hideorshow = 'eye-off';
    } else {
      this.type = 'password';
      this.hideorshow = 'eye';
    }
  }

}
