import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/util/alert.service';

import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-password-recover',
  templateUrl: './password-recover.page.html',
  styleUrls: ['./password-recover.page.scss'],
})
export class PasswordRecoverPage implements OnInit {
  titulo = 'Recuperar Contraseña';

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public auth: AuthService,
    private router: Router,
    private storage: Storage,
    private alertService: AlertService
  ) { }

  ngOnInit() {
  }

  regresar() {
    this.router.navigate(['login']);
  }


  enviarContrasena() {
    alert("Contrasena enviada")
  }

}
