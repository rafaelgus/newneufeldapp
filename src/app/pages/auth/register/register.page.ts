import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/util/alert.service';

import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  titulo = 'Nuevo Registro';

  public loading;
  public form: any = [];
  er: any;


  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public auth: AuthService,
    private router: Router,
    private storage: Storage,
    private alertService: AlertService
  ) { }

  ngOnInit() {
  }

  registrarse() {
    if (
      this.form.name !== undefined &&
      this.form.name !== '' &&
      this.form.email !== undefined &&
      this.form.email !== '' &&
      this.form.password !== undefined &&
      this.form.password !== '' &&
      this.form.password2 !== undefined &&
      this.form.password2 !== '' &&
      this.form.telefono !== undefined &&
      this.form.telefono !== '' &&
      this.form.whatsapp !== undefined &&
      this.form.whatsapp !== '' &&
      this.form.direccion !== undefined &&
      this.form.direccion !== '' &&
      this.form.ruc !== undefined &&
      this.form.ruc !== ''
    ) {
      if (this.form.password === this.form.password2) {
        const user = this.convertirData();
        this.auth.registro(user).subscribe(
          (data) => {
            if (data.response.sucess === 0) {
              this.alertService.presentToast('El correo existe');
            } else {
              this.guardarUsuario(data.response.user);
            }
          },
          (error) => {
            this.alertService.presentToast(error.statusText);
            this.er = error.statusText;
          }
        );
      } else {
        this.alertService.presentToast('No coinciden las contraseñas');
      }
    } else {
      this.alertService.presentToast('Todos los campos son requeridos');
    }
  }

  guardarUsuario(user) {
    this.storage.set('user', user).then(() => {
      this.router.navigate(['menu']);
    });
  }

  convertirData(): any {
    return {
      name: this.form.name,
      email: this.form.email,
      telefono: this.form.telefono,
      whatsapp: this.form.whatsapp,
      password: this.form.password,
      direccion: this.form.direccion,
      ruc: this.form.ruc,
      rol: 2
    };
  }

  regresar() {
    this.router.navigate(['login']);
  }

}
