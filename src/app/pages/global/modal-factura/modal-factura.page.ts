import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-factura',
  templateUrl: './modal-factura.page.html',
  styleUrls: ['./modal-factura.page.scss'],
})
export class ModalFacturaPage implements OnInit {
  private user: any = [];
  formgroup: FormGroup;
  nombrefac: AbstractControl;
  direccionfac: AbstractControl;
  telefonofac: AbstractControl;
  emailfac: AbstractControl;
  rucfac: AbstractControl;
  constructor(
    public storage: Storage,
    public formBuilder: FormBuilder,
    private alertService: AlertService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private modalCtrl: ModalController
  ) {
    this.formgroup = formBuilder.group({
      nombrefac: [
        this.user.name,
        Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])
      ],
      direccionfac: [
        this.user.direccion,
        Validators.compose([Validators.maxLength(60), Validators.required])
      ],
      telefonofac: [this.user.telefono, Validators.required],
      emailfac: [
        this.user.email,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ],
      rucfac: [this.user.ruc, Validators.required]
    });

    this.nombrefac = this.formgroup.controls['nombrefac'];
    this.direccionfac = this.formgroup.controls['direccionfac'];
    this.telefonofac = this.formgroup.controls['telefonofac'];
    this.emailfac = this.formgroup.controls['emailfac'];
    this.rucfac = this.formgroup.controls['rucfac'];
  }

  ngOnInit() {
    this.alertService.presentToast('Por favor completar los campos para emitir la factura');
    this.storage.get('user').then((val) => {
      this.user = val;
    });
  }
  closeModal() {
    const data = {
      nombrefac: this.user.name,
      rucfac: this.user.ruc,
      telefonofac: this.user.telefono,
      emailfac: this.user.email,
      direccionfac: this.user.direccion
    };
    this.alertService.presentToast('Datos de Facturacion guardados correctamente');
    this.modalCtrl.dismiss(data);
  }

  backModal() {
    this.modalCtrl.dismiss();
  }
}
