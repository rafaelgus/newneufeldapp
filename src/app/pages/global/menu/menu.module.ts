

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

import { LandingPageModule } from './../../user/dashboard/landing/landing.module';
import { OrderHistoryPageModule } from '../../user/dashboard/order/order-history/order-history.module';
import { TrackOrderPageModule } from '../../user/dashboard/order/track-order/track-order.module';
import { UserConfigPageModule } from '../../user/dashboard/user-config/user-config.module';
import { CategoryPageModule } from '../../user/dashboard/product/category/category.module';
import { CategoryDetailsPageModule } from '../../user/dashboard/product/category-details/category-details.module';
import { TrackOrderListPageModule } from '../../user/dashboard/order/track-order-list/track-order-list.module';
import { ProductDetailsPageModule } from '../../user/dashboard/product/product-details/product-details.module';
import { RequestDataPageModule } from '../../user/dashboard/order/request-data/request-data.module';
import { RequestDetailsPageModule } from '../../user/dashboard/order/request-details/request-details.module';
import { OrderHistoryDetailsPageModule } from '../../user/dashboard/order/order-history-details/order-history-details.module';
import { MapsNativePageModule } from '../../user/dashboard/order/maps-native/maps-native.module';
import { FaqPageModule } from '../faq/faq.module';
import { SoportePageModule } from '../soporte/soporte.module';
import { SubCategoryDetailsPageModule } from '../../user/dashboard/product/sub-category-details/sub-category-details.module';
import { SubCategoryPageModule } from '../../user/dashboard/product/sub-category/sub-category.module';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'landing',
        loadChildren: () => LandingPageModule
      },
      {
        path: 'orderhistory',
        loadChildren: () => OrderHistoryPageModule
      },
      {
        path: 'orderhistorydetail/:order',
        loadChildren: () => OrderHistoryDetailsPageModule
      },
      {
        path: 'trackorderlist',
        loadChildren: () => TrackOrderListPageModule
      },
      {
        path: 'track/:id',
        loadChildren: () => TrackOrderPageModule

      },
      {
        path: 'userconfig',
        loadChildren: () => UserConfigPageModule
      },
      {
        path: 'categorias',
        loadChildren: () => CategoryPageModule
      },
      {
        path: 'catdetalle/:id/:subid',
        loadChildren: () => CategoryDetailsPageModule
      },
      {
        path: 'subcat/:id',
        loadChildren: () => SubCategoryPageModule
      },
      {
        path: 'proddetalle/:id',
        loadChildren: () => ProductDetailsPageModule
      },
      {
        path: 'reqsdata',
        loadChildren: () => RequestDataPageModule
      },
      {
        path: 'reqsdetails',
        loadChildren: () => RequestDetailsPageModule
      },
      {
        path: 'mapsnative/:id',
        loadChildren: () => MapsNativePageModule
      },
      {
        path: 'faq',
        loadChildren: () => FaqPageModule
      },
      {
        path: 'soporte',
        loadChildren: () => SoportePageModule
      }

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule { }
