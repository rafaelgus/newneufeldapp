import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { OrderService } from 'src/app/services/order/order.service';
import { Storage } from '@ionic/storage';
import { IonInfiniteScroll, LoadingController } from '@ionic/angular'

@Component({
  selector: 'app-track-order-list',
  templateUrl: './track-order-list.page.html',
  styleUrls: ['./track-order-list.page.scss'],
})
export class TrackOrderListPage implements OnInit {
  titulo = 'Ordenes en camino';

  private loading: any;
  user: any = [];
  ordenes: any = [];
  private skip = 0;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private order: OrderService,
    public loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.mostrarProgressBar('Loading...');
    this.storage.get('user').then((resp) => {
      this.user = resp;
      this.order.misOrdenes(this.user.id, this.skip).subscribe((resp) => {
        if (resp.sucess === 1) {
          this.ordenes = resp.ordenes;
          console.log(this.ordenes);
        } else {
          this.alertService.presentToast('Sin Ordenes para mostrar');
        }
      });
    });
  }
  rastrearOrden(orden) {
    this.router.navigate(['menu/mapsnative/:id', { id: orden.id }]);
    this.storage.set('orden', orden);
  }

  cadeteMapa(orden) {
    this.router.navigate(['ruta-orden/:id', { id: orden.id }]);
    this.storage.set('orden', orden);
  }

  doInfinite(infiniteScroll) {
    this.skip += 15;
    this.order.misOrdenes(this.user.id, this.skip).subscribe((resp) => {
      let items = resp.ordenes;

      if (items !== undefined) {
        for (let item of items) {
          this.ordenes.push(item);
        }
      } else {
        this.alertService.presentToast('No hay mas ordenes');
      }

      infiniteScroll.complete();
    });
  }

  async mostrarProgressBar(message) {
    this.loading = await this.loadingCtrl.create({
      message: message,
      spinner: 'crescent',
      duration: 2000
    });
    return await this.loading.present();
  }

}
