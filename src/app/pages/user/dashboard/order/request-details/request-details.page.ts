import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { global } from 'src/app/services/util/global';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators, FormControlName, AbstractControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.page.html',
  styleUrls: ['./request-details.page.scss'],
})
export class RequestDetailsPage implements OnInit {
  platillos: any = [];
  listPedido: any = [];
  detallestramite: any;
  subTotal = 0;
  total = 0;








  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private ngZone: NgZone,
    public formBuilder: FormBuilder,
  ) {


  }

  ngOnInit() {
    this.storage.get('pedido').then((resp) => {
      this.listPedido = resp;

      if (this.listPedido !== undefined) {
        for (const element of this.listPedido) {
          this.subTotal = this.listPedido.reduce((a, b) => a + b.cantidad * b.precio, 0);
          this.storage.set('subtotal', this.subTotal);
          console.log(this.subTotal);
        }
      }
    });

  }
  ionViewWillEnter() {
    if (global.detallesTramite != null) {
      this.listPedido[global.index].avisos = global.detallesTramite;
      this.alertService.presentToast('Details on saved ');
    }
  }

  verDetalle(pedido) {
    this.alertService.presentToast(pedido.avisos);
  }
  continuarPedido() {
    console.log(this.listPedido);
    this.storage.remove('pedido');
    this.storage.set('pedido', this.listPedido);
    this.router.navigate([
      'reqsdata'

    ]);


  }

  async eliminar(pedido) {
    const index = await this.listPedido.indexOf(pedido, 0);
    if (index > -1) {
      this.listPedido.splice(index, 1);
    }

    await this.storage.set('pedido', this.listPedido);

    this.afterDelete();
  }

  afterDelete() {
    this.storage.get('pedido').then((resp) => {
      this.listPedido = resp;

      if (this.listPedido !== undefined) {
        this.total = this.listPedido.reduce((a, b) => a + b.cantidad * b.precio, 0);
        console.log(this.total);
        for (const element of this.listPedido) {
          this.subTotal = this.listPedido.reduce((a, b) => a + b.cantidad * b.precio, 0);
          this.storage.remove('subtotal');
          this.storage.set('subtotal', this.subTotal);
        }
      }
      if (this.listPedido.length === 0) {
        this.router.navigate(['menu/categorias']);
      }
    });
  }

  onChangeTime(pedido) {
    this.listPedido.push(pedido);
    var index = this.platillos.indexOf(pedido, 0);
    if (index > -1) {
      this.platillos.splice(index, 1);
    }
    var index = this.listPedido.indexOf(pedido, 0);
    if (index > -1) {
      this.listPedido.splice(index, 1);
    }
    this.storage.set('pedido', this.listPedido);
    this.alertService.presentToast('Detalles guardados ');
    this.afterDelete();
    /* this.storage.get('pedido').then((resp) => {
      this.listPedido = resp;

      if (this.listPedido !== undefined) {
        for (const element of this.listPedido) {
          this.subTotal = this.listPedido.reduce((a, b) => a + b.cantidad * b.precio, 0);
          this.storage.set('subtotal', this.subTotal);
          console.log(this.subTotal);
        }
      }
    }); */
  }

  async sumar(pedido) {
    console.log('entrando:', pedido);
    pedido.cantidad += 1;
    this.subTotal += +pedido.precio;


    this.storage.remove('subtotal');
    this.storage.set('subtotal', this.subTotal);
    console.log('modificado', this.listPedido);

  }

  restar(pedido) {
    if (pedido.cantidad > 0) {
      pedido.cantidad -= 1;
      this.subTotal -= +pedido.precio;
      this.storage.remove('subtotal');

      this.storage.set('subtotal', this.subTotal);

      console.log('modificado', this.listPedido);

    }
  }

  modificacionSumRest(pedido) {
    this.listPedido.push(pedido);
    var index = this.platillos.indexOf(pedido, 0);
    if (index > -1) {
      this.platillos.splice(index, 1);
    }
    var index = this.listPedido.indexOf(pedido, 0);
    if (index > -1) {
      this.listPedido.splice(index, 1);
    }
    this.storage.set('pedido', this.listPedido);
    this.alertService.presentToast('Detalles guardados ');
    this.afterDelete();
    this.storage.get('pedido').then((resp) => {
      this.listPedido = resp;

      if (this.listPedido !== undefined) {
        for (const element of this.listPedido) {
          this.subTotal = this.listPedido.reduce((a, b) => a + b.cantidad * b.precio, 0);
          this.storage.set('subtotal', this.subTotal);
          console.log(this.subTotal);
        }
      }
    });
  }

  back() {
    this.router.navigate(['menu/categorias']);
  }

}
