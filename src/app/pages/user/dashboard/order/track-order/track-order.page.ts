import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';

declare var google;

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.page.html',
  styleUrls: ['./track-order.page.scss'],
})
export class TrackOrderPage implements OnInit, AfterViewInit {
  titulo = 'Rastrea tu orden ';
  map;
  @ViewChild('mapElement') mapNativeElement: ElementRef;
  latitude: any;
  longitude: any;

  constructor(private geolocation: Geolocation, private storage: Storage) { }

  ngOnInit() {

    /* this.storage.get('position').then((resp) => {
      console.log(resp);
      this.latitude = resp.lat;
      this.longitude = resp.lng;
    });
    this.showmap(); */
  }

  ngAfterViewInit(): void {

    this.storage.get('position').then((resp) => {
      console.log(resp);
      this.latitude = resp.lat;
      this.longitude = resp.lng;
      this.showmap();
    });

  }

  showmap() {
    this.storage.get('position').then((resp) => {
      this.latitude = resp.lat;
      this.longitude = resp.lng;
      const mapOptions = {
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
      };
      const map = new google.maps.Map(this.mapNativeElement.nativeElement, mapOptions);

      const infoWindow = new google.maps.InfoWindow;
      const pos = {
        lat: this.latitude,
        lng: this.longitude
      };
      const marker = new google.maps.Marker({
        position: pos,
        map,
        title: 'Marcador 1',

      });
      infoWindow.setPosition(pos);
      infoWindow.setContent('Estas Aqui ');

      map.setCenter(pos);
      marker.addListener('click', function () {
        infoWindow.open(map, marker);
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

}
