import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertService } from 'src/app/services/util/alert.service';
import { OrderService } from 'src/app/services/order/order.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.page.html',
  styleUrls: ['./order-history.page.scss'],
})
export class OrderHistoryPage implements OnInit {
  titulo = 'Tus Ordenes';
  private loading: any;
  user: any = [];
  ordenes: any = [];
  private skip = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private order: OrderService,
    public loadingCtrl: LoadingController


  ) { }

  ngOnInit() {
    this.mostrarProgressBar('Loading...');
    this.storage.get('user').then((resp) => {
      this.user = resp;
      this.order.misOrdenes(this.user.id, this.skip).subscribe((resp) => {
        /* 	this.loading.dismiss(); */
        if (resp.sucess === 1) {
          this.ordenes = resp.ordenes;
        } else {
          this.alertService.presentToast('Sin Ordenes para mostrar');
        }
      });
    });
  }

  historialPedido(orden) {
    this.router.navigate(['menu/orderhistorydetail/:id', { id: orden.id }]);
  }

  doInfinite(infiniteScroll) {
    this.skip += 15;
    this.order.misOrdenes(this.user.id, this.skip).subscribe((resp) => {
      let items = resp.ordenes;

      if (items !== undefined) {
        for (let item of items) {
          this.ordenes.push(item);
        }
      } else {
        this.alertService.presentToast('No hay mas ordenes');
      }

      infiniteScroll.complete();
    });
  }

  async mostrarProgressBar(message) {
    this.loading = await this.loadingCtrl.create({
      message,
      spinner: 'crescent',
      duration: 2000
    });
    return await this.loading.present();
  }

}
