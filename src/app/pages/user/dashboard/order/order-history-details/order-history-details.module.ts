import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderHistoryDetailsPage } from './order-history-details.page';

const routes: Routes = [
  {
    path: '',
    component: OrderHistoryDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderHistoryDetailsPage]
})
export class OrderHistoryDetailsPageModule {}
