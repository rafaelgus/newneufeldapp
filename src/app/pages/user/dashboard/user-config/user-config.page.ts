import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';

@Component({
  selector: 'app-user-config',
  templateUrl: './user-config.page.html',
  styleUrls: ['./user-config.page.scss'],
})
export class UserConfigPage implements OnInit {
  titulo = 'Configura tu cuenta';
  user: any = [];
  public loading;
  public form: any = [];
  private user_id: any;

  constructor(
    public storage: Storage,
    public formBuilder: FormBuilder,
    private alertService: AlertService,
    private auth: AuthService,
    private router: Router

  ) { }

  ngOnInit() {
    this.storage.get('user').then((val) => {
      this.user = val;
      this.user_id = this.user.id;
      this.form.name = this.user.name;
      this.form.email = this.user.email;
      this.form.telefono = this.user.telefono;
      this.form.direccion = this.user.direccion;
      this.form.password = this.user.password;
      this.form.ruc = this.user.ruc;
      this.form.whatsapp = this.user.whatsapp;
    });
  }

  updateUser() {
    const user = this.convertirData();
    this.auth.update(this.user_id, user).subscribe((resp) => {
      if (resp.sucess === 0) {
        this.alertService.presentToast('Error!!');
      } else {
        this.regresarApp(user);
      }
    });
  }

  convertirData(): any {
    return {
      id: this.user_id,
      rol: this.user.rol,
      lat: this.user.lat,
      lng: this.user.lng,
      name: this.form.name,
      direccion: this.form.direccion,
      email: this.form.email,
      telefono: this.form.telefono,
      whatsapp: this.form.whatsapp,
      ruc: this.form.ruc,
      password: this.form.password
    };
  }

  regresarApp(user) {
    this.alertService.presentToast('Datos guardados satisfactoriamente');
    this.storage.remove('user');
    this.storage.set('user', user).then(() => {
      this.router.navigate(['menu/landing']);
    });
  }


}
