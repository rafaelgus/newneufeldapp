import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router, RouterEvent } from '@angular/router';
import { GlobalService } from 'src/app/services/global/global.service';
import { ModalController } from '@ionic/angular';
import { ImageModalPage } from 'src/app/pages/global/image-modal/image-modal.page';
import { environment } from 'src/environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AuthService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.page.html',
  styleUrls: ['./start-page.page.scss'],
})
export class StartPagePage implements OnInit, AfterContentInit {
  titulo = 'Inicio';
  private SERVER: string = environment.server;
  private user;
  sliderOpts = {
    zoom: false,
    slidesPerView: 1.5,
    spaceBetween: 20,
    centeredSlides: true,
    autoplay: true,
    loop: true,
    speed: 500,
    effect: 'flip'
  };
  sliders: any;
  destacados: any;
  galleryType = 'pinterest';
  listPedido: any = [];

  constructor(
    private router: Router,
    private storage: Storage,
    private global: GlobalService,
    private modalController: ModalController,
    private geolocation: Geolocation,
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.global.getSliders().subscribe((data) => {
      this.sliders = data;
    });

    this.global.getDestacados().subscribe((data) => {
      this.destacados = data.response.platillos;
    });




  }

  ngAfterContentInit() {

    this.storage.get('user').then((val) => {
      this.user = val;
      const id = this.user.id;
      this.enviarUbicacion(id);
    });
  }

  async getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }

  openPreview(img) {
    this.modalController
      .create({
        component: ImageModalPage,
        componentProps: {
          img: img.imagen
        }
      })
      .then((modal) => {
        modal.present();
      });
  }

  goToCategories() {
    this.router.navigateByUrl('/menu/categorias');
  }

  openProducto(image) {
    this.router.navigate(['menu/proddetalle/', image.id]);
  }

  enviarUbicacion(id) {
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      const coords = { lat: data.coords.latitude, lng: data.coords.longitude };
      this.storage.set('position', coords);
      this.auth.enviarMiPosicion(id, coords);
      /* this.auth.enviarMiPosicion(this.user.id, coords) */


    });
  }

}
