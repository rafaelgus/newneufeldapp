

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LandingPage } from './landing.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { UserConfigPageModule } from '../user-config/user-config.module';
import { TrackOrderPageModule } from './../order/track-order/track-order.module';
import { StartPagePageModule } from '../start-page/start-page.module';
import { TrackOrderListPageModule } from '../order/track-order-list/track-order-list.module';
import { CategoryPageModule } from '../product/category/category.module';
import { RequestDetailsPageModule } from '../order/request-details/request-details.module';
import { MenuPageModule } from 'src/app/pages/global/menu/menu.module';

const routes: Routes = [
  {
    path: 'tabs',
    component: LandingPage,
    children: [
      {
        path: 'home',
        loadChildren: () => StartPagePageModule
      },
      {
        path: 'config',
        loadChildren: () => UserConfigPageModule
      },
      {
        path: 'tracklist',
        loadChildren: () => TrackOrderListPageModule
      },
      {
        path: 'cat',
        loadChildren: () => CategoryPageModule
      },
      {
        path: 'cart',
        loadChildren: () => RequestDetailsPageModule
      },


    ]
  },
  {
    path: '',
    redirectTo: 'tabs/home',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [LandingPage]
})
export class LandingPageModule { }
