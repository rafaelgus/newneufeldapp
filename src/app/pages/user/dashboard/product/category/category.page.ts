
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { Router, ActivatedRoute, RouterEvent, NavigationStart } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
  titulo = 'Categorias de Productos'
  categorias: any[];
  skip = 0;
  platillos: any = [];
  listPedido: any = [];
  id: any;
  currentPage: string;
  navigationSubscription;
  constructor(
    private router: Router,
    private storage: Storage,
    private alertService: AlertService,
    private global: GlobalService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationStart) {
        console.log(e);
        this.ngOnInit;
      }
    });
  }


  ngOnInit() {
    this.getpedido();
    this.global.categorias(this.skip).subscribe((data) => {

      if (data.response.sucess === 1) {
        this.categorias = data.response.categorias;
      }
    });
  }

  ionViewDidEnter() {

    this.getpedido();
  }

  ionViewWillLeave() {

    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  async getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }

  itemSelected(categoria) {
    this.id = categoria.id;
    this.router.navigate(['menu/subcat/', this.id]);
  }

  doInfinite(infiniteScroll) {
    this.getpedido();
    this.skip += 15;
    this.global.categorias(this.skip).subscribe((data) => {
      if (data.response.sucess === 1) {
        const items = data.response.categorias;
        for (let item of items) {
          this.categorias.push(item);
        }
      }
      infiniteScroll.complete();
    });
  }

  pedido() {
    this.getpedido();
    if (this.listPedido.length === 0) {
      this.alertService.presentToast('No hay productos para mostrar');
    } else {
      this.router.navigate(['menu/reqsdetails/']);
    }
  }

}
