
import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { Storage } from '@ionic/storage';
import { ProductDetailsPage } from 'src/app/pages/user/dashboard/product/product-details/product-details.page';
import { IonInfiniteScroll, PopoverController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.page.html',
  styleUrls: ['./category-details.page.scss'],
})
export class CategoryDetailsPage implements OnInit {
  titulo = 'realizar pedido';

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  SERVER: string = environment.server;
  myInput: string;
  platillos: any = [];
  listPedido: any = [];
  skip = 0;
  cantidad: string[];
  especificacion: string[];
  detallesProductos: any;
  id: string;
  subid: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private global: GlobalService,
    public popoverCtrl: PopoverController
  ) {
    this.detallesProductos = ProductDetailsPage;
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.subid = this.route.snapshot.paramMap.get('subid');
    console.log(this.subid);


    this.global.categoria(this.id).subscribe((data) => {
      console.log(data);
      if (data.response.sucess === 1) {
        this.filtro(data.response.platillos);
      }
    });
    this.storage.get('pedido').then((resp) => {
      if (resp !== null) {
        this.listPedido = resp;
      }
    });
  }

  onInput() {
    this.platillos = [];
    if (this.myInput !== '') {
      this.id = this.route.snapshot.paramMap.get('id');
      this.global.busquedaPorCategoria(this.myInput, this.id).subscribe((data) => {
        this.filtro(data.response.platillos);
      });
    } else {
      this.id = this.route.snapshot.paramMap.get('id');
      this.global.categoria(this.id).subscribe((data) => {
        if (data.response.sucess === 1) {
          this.filtro(data.response.platillos);
        }
      });
    }
  }

  getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }

  agregar(plato) {
    plato.cantidad = 1;

    this.listPedido.push(plato);
    const index = this.platillos.indexOf(plato, 0);
    if (index > -1) {
      this.platillos.splice(index, 1);
    }

    this.storage.set('pedido', this.listPedido);
  }

  producto(plato) {
		/* 	this.navCtrl.push('ProductsPage', {
			id: plato.id
		}); */
    const id = plato.id;
    this.router.navigate(['menu/proddetalle/', id]);
  }

  pedido(listPedido) {
		/* this.navCtrl.push('DetallePedidoPage', {
			listPedido: this.listPedido
		}); */
    this.router.navigate(['menu/reqsdetails/']);
  }

  filtro(platillos) {
    let isAgregado: boolean = false;

    for (let item of platillos) {
      isAgregado = false;

      if (this.listPedido != null) {
        for (let plato of this.listPedido) {
          if (item.id === plato.id) {
            isAgregado = true;
          }
        }
      } else {
        for (let plato of this.listPedido) {
          isAgregado = false;
        }
      }

      if (!isAgregado) {
        this.platillos.push(item);
      }
    }
  }

  backButton() {
    this.router.navigate(['menu/subcat/', this.id]);
  }

  doInfinite(infiniteScroll) {
    this.skip += 15;
    this.global.platillos(this.skip).subscribe((data) => {
      const items = data.response.platillos;
      if (items !== undefined) {
        for (const item of items) {
          this.platillos.push(item);
        }
      } else {
        this.alertService.presentToast('No hay mas productos para mostrar');
      }
      infiniteScroll.target.complete();
    });
  }

  async presentPopover(ev, data) {
    /*  const popover = await this.popoverCtrl.create({
       component: PopInfoComponent,
       event: ev,
       translucent: false,
       mode: 'ios',
       componentProps: {
         descrp: data.descripcion
       }
     }); */

    /* return await popover.present(); */

		/* 	const { data } = await popover.onDidDismiss();
		console.log('por aqui se va la data:', data); */
  }

}
