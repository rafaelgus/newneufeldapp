import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SubCategoryDetailsPage } from './sub-category-details.page';

const routes: Routes = [
  {
    path: '',
    component: SubCategoryDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SubCategoryDetailsPage]
})
export class SubCategoryDetailsPageModule {}
