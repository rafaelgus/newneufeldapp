import { Component, OnInit } from '@angular/core';

import { GlobalService } from 'src/app/services/global/global.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';
import { ProductDetailsPageModule } from './product-details.module';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  SERVER: string = environment.server;
  myInput: string;
  platillos: any = [];
  producto: any = [];
  listPedido: any = [];
  skip = 0;
  cantidad = 0;
  especificacion: string;
  medi: any = [];
  id: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private global: GlobalService
  ) { }

  ngOnInit() {
    this.getpedido();
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    console.log('cantidad', this.cantidad);
    this.global.getPlatillo(this.id).subscribe((data) => {
      if (data.response.sucess === 1) {
        this.producto = data.response.platillos;
      }
    });
  }
  getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }

  pedido(listPedido) {
		/* this.navCtrl.push('DetallePedidoPage', {
			listPedido: this.listPedido
		}); */
    this.router.navigate(['menu/reqsdetails/']);
  }

  agregar(producto) {
    if (this.cantidad === undefined) {
      producto.cantidad = 1;
    } else {
      producto.cantidad = this.cantidad;
    }

    this.listPedido.push(producto);
    const index = this.platillos.indexOf(producto, 0);
    if (index > -1) {
      this.platillos.splice(index, 1);
    }

    this.storage.set('pedido', this.listPedido);
    this.alertService.presentToast('El producto se ha agregado a tu orden ');
    this.router.navigate(['menu/categorias']);
  }

  sumar(producto) {
    this.cantidad += 1;
    producto.cantidad = this.cantidad;


  }

  restar(producto) {
    if (this.cantidad > 0) {
      this.cantidad -= 1;
      producto.cantidad = this.cantidad;
    }
  }

}
