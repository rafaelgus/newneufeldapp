export interface Componente {
  icon: string;
  name: string;
  redirecTo: string;
}

export interface Tabs {
  icon: string;
  name: string;
  redirecTo: string;
}
