import { Injectable } from '@angular/core';


import { Observable, pipe } from 'rxjs';


import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, LoadingController } from '@ionic/angular';
import * as Util from 'src/app/services/util/util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  httpHeaders = new HttpHeaders();
  requestObject: any = null;
  endpoint = Util.API_ENDPOINT;

  constructor(
    private http: HttpClient,
    private plt: Platform,
    private loadingCtrl: LoadingController
  ) {
    this.httpHeaders.append('Content-Type', 'application/json');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  realizarOrden(data): Observable<any> {
    return this.http
      .post(this.endpoint + Util.ORDEN, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  misOrdenes(id, skip): Observable<any> {
    return this.http
      .get(this.endpoint + Util.MIS_ORDENES + '/' + id + '/' + skip, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  detalle_orden(orden_id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.ORDEN + '/' + orden_id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  setEstadoOrden(orden_id, estado): Observable<any> {
    return this.http
      .get(this.endpoint + Util.CAMBIAR_ESTADO + '/' + orden_id + '/' + estado, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  setPago(data): Observable<any> {
    return this.http
      .post(this.endpoint + Util.CAMBIAR_PAGO, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  encargos(id, skip): Observable<any> {
    return this.http
      .get(this.endpoint + Util.ENCARGOS + '/' + id + '/' + skip, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  formasdepago(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.FORMAS_PAGO, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  envio(skip): Observable<any> {
    return this.http
      .get(this.endpoint + Util.ENVIOS + skip, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getOrden(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.ORDEN, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }
}
