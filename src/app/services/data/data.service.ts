import { Componente, Tabs } from 'src/app/interfaces/interfaces';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getMenuOpts() {
    return this.http.get<Componente[]>('/assets/data/menu.json');
  }

  getTabsOpts() {
    return this.http.get<Tabs[]>('/assets/data/tabs.json');
  }
}
