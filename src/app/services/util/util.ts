import { environment } from 'src/environments/environment';

export const API_ENDPOINT = environment.server + '/api';
export const USER = '/user';
export const UPDATE = '/update';
export const LOGIN = '/login';
export const PLATILLOS = '/platillos';
export const GETPLATILLO = '/platillos/search';
export const ORDEN = '/orden';
export const MIS_ORDENES = '/misOrdenes';
export const ENCARGOS = '/encargos';
export const CAMBIAR_UBICACION = '/cambiarPosicion/';
export const UBICACION = '/ubicacion/';
export const CAMBIAR_ESTADO = '/cambiarEstado';
export const BUSCAR = '/busqueda/';
export const CATEGORIAS = '/categorias/';
export const CATEGORIA = '/categoria/';
export const SUBCATEGORIA = '/subcategorias/';
export const FORMAS_PAGO = '/formasdepago';
export const CAMBIAR_PAGO = '/cambiarpago';
export const ENVIOS = '/envios/';
export const SUCURSALES = '/sucursales';
export const CONFIG = '/configuraciones';
export const FAQ = '/faq';

export const SLIDERS = '/sliders';
export const DESTACADOS = '/platillos/destacados';
export const ADDRESSORDER = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

export const GOOGLE_DIST = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
export const GOOGLE_DESTINATIONS = '&destinations=-2.165844,-79.917904&key=AIzaSyBQ4kJ79IJLEM4iki6aqrE_s1gsPtRYlCQ';
export const GOOGLE_DISTANCES = '&key=AIzaSyBQ4kJ79IJLEM4iki6aqrE_s1gsPtRYlCQ';
export const GOOGLEADRS = '&key=AIzaSyBQ4kJ79IJLEM4iki6aqrE_s1gsPtRYlCQ';