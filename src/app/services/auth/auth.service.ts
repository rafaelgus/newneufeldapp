import { Injectable } from '@angular/core';


import { Observable, pipe } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, LoadingController } from '@ionic/angular';
import * as Util from 'src/app/services/util/util';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  httpHeaders = new HttpHeaders();
  requestObject: any = null;
  endpoint = Util.API_ENDPOINT;

  constructor(
    private http: HttpClient,
    private plt: Platform,
    private loadingCtrl: LoadingController
  ) {
    this.httpHeaders.append('Content-Type', 'application/json');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  registro(data): Observable<any> {
    return this.http
      .post(this.endpoint + Util.USER, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  update(id, data): Observable<any> {
    return this.http
      .post(this.endpoint + Util.USER + '/' + id + Util.UPDATE, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  login(data): Observable<any> {
    return this.http
      .post<any>(this.endpoint + Util.USER + Util.LOGIN, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  enviarMiPosicion(id, data): Observable<any> {
    return this.http
      .post(this.endpoint + Util.USER + Util.CAMBIAR_UBICACION + id, data, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }
}
