import { Injectable } from '@angular/core';

import { Observable, pipe } from 'rxjs';


import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, LoadingController } from '@ionic/angular';
import * as Util from 'src/app/services/util/util';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  httpHeaders = new HttpHeaders();
  requestObject: any = null;
  endpoint = Util.API_ENDPOINT;

  constructor(
    private http: HttpClient,
    private plt: Platform,
    private loadingCtrl: LoadingController
  ) {
    this.httpHeaders.append('Content-Type', 'application/json');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  categorias(skip): Observable<any> {
    return this.http
      .get(this.endpoint + Util.CATEGORIAS + skip, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  categoria(id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.CATEGORIA + id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  subcategoria(id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.SUBCATEGORIA + id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  busquedaPorCategoria(nombre, categoria_id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.PLATILLOS + Util.BUSCAR + nombre + '/' + categoria_id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  platillos(id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.PLATILLOS + '/' + id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getPlatillo(id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.GETPLATILLO + '/' + id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getSucursales(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.SUCURSALES, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getSliders(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.SLIDERS, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getconfig(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.CONFIG, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getfaq(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.FAQ, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getDestacados(): Observable<any> {
    return this.http
      .get(this.endpoint + Util.DESTACADOS, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }
  getGeoCodefromGoogleAPI(address: string): Observable<any> {
    return this.http.get(Util.ADDRESSORDER + address + Util.GOOGLEADRS).pipe(map(this.extractData));
  }

  getUbicacion(id): Observable<any> {
    return this.http
      .get(this.endpoint + Util.UBICACION + id, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }

  getDistance(direccion): Observable<any> {
    return this.http.get(direccion).pipe(map(this.extractData));
  }

  buscar(nombre): Observable<any> {
    return this.http
      .get(this.endpoint + Util.PLATILLOS + Util.BUSCAR + nombre, {
        headers: this.httpHeaders
      })
      .pipe(map(this.extractData));
  }
}
