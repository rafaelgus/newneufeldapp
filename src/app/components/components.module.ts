
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TabsComponent } from './tabs/tabs.component';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { SecondHeaderComponent } from './second-header/second-header.component';


@NgModule({
  declarations: [
    HeaderComponent,
    TabsComponent,
    MenuComponent,
    SecondHeaderComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    TabsComponent,
    MenuComponent,
    SecondHeaderComponent
  ]
})
export class ComponentsModule { }
